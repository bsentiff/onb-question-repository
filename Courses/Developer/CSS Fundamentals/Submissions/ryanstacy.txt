(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): CSS Fundamentals
(Course URL): http://www.lynda.com/Web-Interactive-CSS-tutorials/CSS-Fundamentals/80436-2.html
(Discipline): CSS
(ENDIGNORE)

(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): How was HTML displayed before CSS? 
(A): It wasn't
(B): Every browser had a default style sheet
(C): You could only have text
(D): The styles were written inline
(Correct): B
(Points): 1
(CF): Every browser had their own form of CSS. When you write CSS, you are overriding this default style.
(WF): Every browser had their own form of CSS. When you write CSS, you are overriding this default style.
(STARTIGNORE)
(Hint): 
(Subject): CSS	
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 1
(Random answers): 1
(Question): Which of the following is a CSS selector?  (Select all that apply)
(A): ul
(B): {}
(C): font-family
(D): .property 
(Correct): A, D
(Points): 1
(CF): ul is a global selector, .property is a class selector.
(WF): ul is a global selector, .property is a class selector.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 1
(Random answers): 1
(Question): What do you need to keep in mind when structuring your HTML? (Select all that apply)
(A): The structure of your HTML can affect the efficiency of your CSS
(B): HTML and CSS are completely independent of each other
(C): CSS class names and IDs are not relevant in HTML
(D): Reuse class names for similar structures
(Correct): A, D
(Points): 1
(CF): HTML and CSS go hand in hand.  You need to be mindful of IDs and class names.
(WF): HTML and CSS go hand in hand.  You need to be mindful of IDs and class names.
(STARTIGNORE)
(Hint): 
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): Browsers read selectors from righ to left.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): It may seem backwards, but right to left is the format that selectors are read.
(WF): It may seem backwards, but right to left is the format that selectors are read.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not an appropriate location for CSS styles?
(A): Database
(B): In-line
(C): External
(D): Server
(Correct): A
(Points): 1
(CF): You should not store CSS in a Database
(WF): You should not store CSS in a Database
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): You can only specify one font per selector.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You can specify multiple fallback fonts.
(WF): You can specify multiple fallback fonts.
(STARTIGNORE)
(Hint): 
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): In the head tag, there is a "a { color: black;}" style listed. In the body tag, there is a "<a href="" style="color:red;"> link </a>.  What color is the link?
(A): Black
(B): Red
(C): Whatever the browser default is
(Correct): B
(Points): 1
(CF): In this case, the last rule applied will be the determining one.
(WF): In this case, the last rule applied will be the determining one.
(STARTIGNORE)
(Hint): 
(Subject): CSS
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a valid unit for font-size
(A): em
(B): px
(C): %
(D): p
(Correct): D
(Points): 1
(CF): p is not a unit for CSS.
(WF): p is not a unit for CSS.
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): True or False - If you don't declare a property, its value is 0.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Remember, Browsers assign default styles!
(WF): Remember, Browsers assign default styles!
(STARTIGNORE)
(Hint):
(Subject): CSS
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 9
(Grade style): 0
(Random answers): 1
(Question): Which of the following position values will allow an element to scroll down with the page?
(A): Fixed
(B): Absolute
(C): Relative 
(D): Static
(Correct): A
(Points): 1
(CF): A Fixed value means that the element will remain in position relative to the view port, regardless of scrolling or resizing. 
(WF): A Fixed value means that the element will remain in position relative to the view port, regardless of scrolling or resizing. 
(STARTIGNORE)
(Hint): 
(Subject): CSS
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

