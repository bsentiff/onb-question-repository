(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Java Advanced Training
(Course URL) : http://www.lynda.com/Java-tutorials/Java-Advanced-Training/107061-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What does peek() do when called on a LinkedList?
(A): Removes the top element
(B): Retrieves but does not remove the top element
(C): Pushes the top element to the bottom of the list
(D): Removes the bottom (or last) element
(E): Retrieves but does not remove the bottom (or last) element
(Correct): B
(Points): 1
(CF): The peek() method retrieves but does not remove the LinkedList's top element.
(WF): The peek() method retrieves but does not remove the LinkedList's top element.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1 
(Grade style): 1
(Random answers): 1
(Question): Which of the following are ways in which you can run an operation on a secondary thread?
(A): Pass a method name into Thread.start()
(B): Define two Main methods
(C): Create a class that implements the Runnable interface and use new Thread(runnable).start()
(D): Create a class that extends the Thread class and override the execute() method, then call the begin() method on an instance of your new class
(E): Create a class that extends the Thread class and override the run() method, then call the start() method on an instance of your new class
(Correct): C, E
(Points): 1
(CF): Of the methods listed, the correct ways to run a separate thread are to implement the runnable interface or extend the Thread class, override run(), and call start()
(WF): Of the methods listed, the correct ways to run a separate thread are to implement the runnable interface or extend the Thread class, override run(), and call start()
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): In which of the following situations would you use Java's Scanner class?
(A): When reading many files for input
(B): When you need advanced regular expression functionality
(C): When reading very large files
(D): When reading tokenized (such as comma-separated) text
(E): When reading a UTF-8 encoded file
(Correct): D
(Points): 1
(CF): Use the Scanner class when reading tokenized (such as comma-separated) text.
(WF): Use the Scanner class when reading tokenized (such as comma-separated) text.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): When calling Thread.sleep, which exception must you handle?
(A): ThreadAbortedException
(B): InterruptedException
(C): SynchronizationException
(D): RunnableException
(E): ContextException
(Correct): B
(Points): 1
(CF): When using Thread.sleep, you must handle InterruptedException.
(WF): When using Thread.sleep, you must handle InterruptedException.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following will NOT compile in Java 7?
(A): int count = 1_000_000;
(B): long count = 100L;
(C): int count = 1_0_0_0_0_0_0;
(D): int count = 1,000;
(E): float count = 1.234F;
(Correct): D
(Points): 1
(CF): You cannot create a numeric literal with a ',' separator.
(WF): You cannot create a numeric literal with a ',' separator.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is the package that contains Java 7's new input/output classes?
(A): java.io
(B): java.files
(C): java.nio
(D): java.paths
(E): java.io7
(Correct): C
(Points): 1
(CF): Java 7's new input/output classes are contained in java.nio.
(WF): Java 7's new input/output classes are contained in java.nio.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What will happen if you call Path filePath = Paths.get("myfile.txt") and myfile.txt does not exist?
(A): The filePath object will still be created
(B): The filePath object will be null
(C): A FileNotFound exception will be thrown at runtime
(D): myfile.txt will be created automatically
(E): An IOException exception will be thrown at runtime
(Correct): A
(Points): 1
(CF): The object will be created, and you can still work with it. Certain methods on the Path instance, however, may throw exceptions if the file doesn't exist.
(WF): The object will be created, and you can still work with it. Certain methods on the Path instance, however, may throw exceptions if the file doesn't exist.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following will compile in Java 7?
(A): LinkedList<String> list = new LinkedList<>();
(B): LinkedList<String, Integer> list = new LinkedList<>();
(C): LinkedList<String> list = new HashMap<String>();
(D): LinkedList<String, Integer> list = new LinkedList<String, Integer>();
(E): LinkedList list = LinkedList.getInstance();
(Correct): A
(Points): 1
(CF): LinkedList<String> list = new LinkedList<>() will compile. It is an example of the new diamond-operator syntax allowed by Java 7.
(WF): LinkedList<String> list = new LinkedList<>() will compile. It is an example of the new diamond-operator syntax allowed by Java 7.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): How are asserts intended to be used?
(A): For developer testing
(B): To sanitize user inputs
(C): To protect against malicious attackers
(D): To ensure strong-typing
(E): To recover from thrown exceptions
(Correct): A
(Points): 1
(CF): The assert keyword is intended for developer use during testing, to notify that a certain condition has occurred.
(WF): The assert keyword is intended for developer use during testing, to notify that a certain condition has occurred.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): How should you create your own exception class? 
(A): Java doesn't allow this
(B): Extend the Exception class
(C): Implement the ErrorCondition interface
(D): Override the Object class's OnError method
(E): Ask Oracle to include it in the next version
(Correct): B
(Points): 1
(CF): To create your own exception classes, simply extend the Exception class.
(WF): To create your own exception classes, simply extend the Exception class.
(STARTIGNORE)
(Hint):
(Subject): Java Advanced Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)