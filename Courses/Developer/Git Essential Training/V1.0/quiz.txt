(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 SQL

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): John Ghidiu
(Course Site): www.lynda.com
(Course Name): Git Essential Training
(Course URL): http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You intend to rename a file which is tracked by git.

What are two methods to rename a file tracked in git? (Choose two)
(A): [code]git mv[/code]
(B): [code]git rename[/code]
(C): Git cannot handle renamed files
(D): Rename the file on disk and stage the addition of the new file and the removal of the old file 
(Correct): A, D
(Points): 1
(CF): git mv and manually renaming the files are both valid methods to rename a valid in git
(WF): git mv and manually renaming the files are both valid methods to rename a valid in git
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): When renaming a tracked file on disk, what will git think happened?
(A): The file was renamed
(B): Git will not see any changes
(C): Git will prompt you to see if the file was renamed
(D): The original file was deleted and the new file was added
(Correct): D
(Points): 1
(CF): Git will recognize that one file was added and one file was removed
(WF): Git will recognize that one file was added and one file was removed
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You perform the following commands:

[code]git add somefile.txt[/code]
[code]git commit -m 'Initial deposit'[/code]
[code]mv somefile.txt otherfile.txt[/code]
[code]git add otherfile.txt[/code]
[code]git rm somefile.txt[/code]

Git recognizes that the file was renamed. This is true if [b]no more than[/b] what percent of the file contents have changed?
(A): About 25%
(B): About 50%
(C): About 75%
(D): About 90%
(Correct): B
(Points): 1
(CF): In general, git can recognize a rename on disk when no more than about 50% of the file content has changed
(WF): In general, git can recognize a rename on disk when no more than about 50% of the file content has changed
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): Should you amend a commit that has been pushed?
(A): No, never
(B): Yes, as long as you notify all users
(C): Only when the push has been in the past 7 days
(D): Yes, if all other users agree it is safe to do so
(Correct): A
(Points): 1
(CF): You should never ammend a commit which has been pushed
(WF): You should never ammend a commit which has been pushed
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)
    

(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You have been developing locally on a branch and have committed a change. You instantly realize that you need to change the commit. You have not pushed the commit yet.

Is it safe to amend the commit?
(A): No, because commits are immutable
(B): Yes, because the commit is in a "rubber" state
(C): Only if no other users are using the same remote
(D): Yes, because integrity will be preserved since there are no descendants whose checksum will change
(Correct): D
(Points): 1
(CF): It is safe to amend the latest commit so long as it has not been pushed
(WF): It is safe to amend the latest commit so long as it has not been pushed
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You have been developing locally on a branch and have committed and pushed a change. You instantly realize that you need to revert the commit.

What procedure should you follow in order to reverse the changes?
(A): It is impossible
(B): Amend the commit
(C): Issue [code]git undo --latest[/code]
(D): Commit and push a changeset which undoes the original change
(Correct): D
(Points): 1
(CF): The only acceptable way to reverse a changeset is to commit a changeset which negates the changes from the original changeset
(WF): The only acceptable way to reverse a changeset is to commit a changeset which negates the changes from the original changeset
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You wish to ignore certain files within the current directory tracked by git.

What is the correct name for the file which specifies the files to ignore?
(A): [code].gitignore[/code]
(B): [code]git.ignore[/code]
(C): [code]gitignore.txt[/code]
(D): [code].git.ignore.txt[/code]
(Correct): A
(Points): 1
(CF): The .gitignore file is used to inform git about files to ignore
(WF): The .gitignore file is used to inform git about files to ignore
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): Which types of files/directories can git track?
(A): Text files
(B): Empty files
(C): Binary files
(D): Empty directories
(Correct): A,B,C
(Points): 1
(CF): Git can track any file, as well as non-empty directories
(WF): Git can track any file, as well as non-empty directories
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): What is the typical file name used for tracking an otherwise empty directory?
(A): .gitkeep
(B): .placeholder
(C): .gitplaceholder
(D): .gitdirectory
(Correct): A
(Points): 1
(CF): The [code].gitkeep[/code] file is convention the convention for tracking empty directories
(WF): The [code].gitkeep[/code] file is convention the convention for tracking empty directories
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You need to track an empty directory, "resources", in git in order to support the requirements of the build tools.

How can you track an empty directory in git?
(A): Add the directory to the index with git add resources
(B): Add the directory to the index with git add --empty resources
(C): Add metadata to the repository indicating where the empty directory resides
(D): Place a file within the "resources" directory and add that file to the index
(Correct): D
(Points): 1
(CF): Any file in a directory will allow the directory to be tracked; the [code].gitkeep[/code] file is used by convention for this purpose
(WF): Any file in a directory will allow the directory to be tracked; the [code].gitkeep[/code] file is used by convention for this purpose
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): What three file types are good candidates to ignore? (Choose three)
(A): Archive files (.zip, .tar)
(B): Source code (.java, .html)
(C): Compiled source (.exe, .com)
(D): OS generated files (.DS_Store)
(E): Images required in the project (.jpg)
(Correct): A, C, D
(Points): 1
(CF): Created artifacts and archive files are good candidates to ignore
(WF): Created artifacts and archive files are good candidates to ignore
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): What is the git stash used for?
(A): Labeling branches for later
(B): The stash is an internal cache for git
(C): Placing files which are no longer needed
(D): Storing changes temporarily without having to commit them to the repository
(Correct): D
(Points): 1
(CF): The stash is used to temporarily store changes without commiting them to the repository
(WF): The stash is used to temporarily store changes without commiting them to the repository
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You wish to include untracked files in the stash.

How do you accomplish this?
(A): You cannot stash untracked files
(B): [code]git stash save -u[/code]
(C): [code]git stash --untracked[/code]
(D): [code]git stash[/code] will automatically stash the files
(Correct): B
(Points): 1
(CF): It is possible to place untracked files in the stash, though they must explicitly be added to the stash
(WF): It is possible to place untracked files in the stash, though they must explicitly be added to the stash
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Advanced
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): Consider the following remotes:

[code]master  http://www.company.com/project.git  ## The master repository[/code]
[code]central http://tfs.company.com/project.git  ## The Team Foundation Server repository[/code]
[code]cd  http://cd.company.com/project.git   ## The continuous delivery server[/code]

Which command will push changes to the "master" branch on the Team Foundation Server repository?
(A): [code]git push master master[/code]
(B): [code]git push central master[/code]
(C): [code]git push "Team Foundation Server" master[/code]
(D): [code]git push http://tfs.company.com/project.git master[/code]
(Correct): B
(Points): 1
(CF): The command correct format for pushing a branch is: git push <remote> <branch>
(WF): The command correct format for pushing a branch is: git push <remote> <branch>
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): How does git facilitate frequent branching?
(A): Branches are inexpensive to create
(B): Git provides automated branch flows
(C): Git prompts for a branch every 5 commits
(D): Git always merges branches and never has collisions
(Correct): A
(Points): 1
(CF): Branches are "cheap" and git encourages frequent branching
(WF): Branches are "cheap" and git encourages frequent branching
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You wish to merge a branch, [code]feature_branch[/code], into the current branch. This merge operation is eligible for a fast-forward merge. However, you wish to keep a historical record of the branch and merge.

Which command will record the fact that a merge occurred?
(A): [code]git ff-merge feature-branch[/code]
(B): [code]git tag --merge feature_branch[/code]
(C): [code]git merge --no-ff feature_branch[/code]
(D): [code]git merge --track-merge feature_branch[/code]
(Correct): C
(Points): 1
(CF): The correct format for a no fast-forward merge is: git merge --no-ff <branch>
(WF): The correct format for a no fast-forward merge is: git merge --no-ff <branch>
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Random answers): 0
(Question): You accidentally pushed a branch "some-cool-feature" to the remote "origin". You would like to remove that branch.

Which command will delete the remote branch?
(A): [code]git push origin :some-cool-feature[/code]
(B): [code]git delete origin some-cool-feature[/code]
(C): [code]git remove origin some-cool-feature[/code]
(D): [code]git push origin --remove some-cool-feature[/code]
(Correct): A
(Points): 1
(CF): The git command to delete a remote branch is: git push <remote> :<branch>
(WF): The git command to delete a remote branch is: git push <remote> :<branch>
(STARTIGNORE)
(Hint):
(Subject): Git
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)