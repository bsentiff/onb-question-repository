(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Lynda
(Course Name): Git Essential Training
(Course URL): https://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): Which answer represents Git best practices for writing commit messages?
(A): Write a long and detailed message with no line breaks
(B): Write a short, single-line summary that describes what the commit does (i.e. ‘Fixes bug…’)
(C): Write a short summary of what you did to solve a problem (i.e. ‘I fixed a bug…’)
(D): Write a short summary of what the commit does, and include instructions for team members in the commit message
(Correct): B
(Points): 1
(CF): The best practices for creating a Git commit message recommend a short, single-line summary (less than 50 characters) that describes what the commit does. Do not reference yourself in the commit message, and keep all message content related to the commit. If you want to include a more detailed commit, use line breaks after the first summary line.
(WF): The best practices for creating a Git commit message recommend a short, single-line summary (less than 50 characters) that describes what the commit does. Do not reference yourself in the commit message, and keep all message content related to the commit. If you want to include a more detailed commit, use line breaks after the first summary line.
(STARTIGNORE)
(Hint):
(Subject): Writing Commit Messages
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): If you want to ignore a file that you were previous tracking, what Git command do you use to untrack (unstage) the file so it can be ignored by the new rule in the .gitignore file?
(A): Nothing, new rules in the .gitignore file will automatically ignore previously tracked files
(B): There is no Git command, delete the file and remake a new file with the same content 
(C): [code]git untrack <filename>[/code]
(D): [code]git rm --cached <filename>[/code]
(Correct): D 
(Points): 1
(CF): Previously tracked files must be unstaged before they will be ignored by a new rule in the .gitignore file. The command to do this is [code]git rm --cached <filename>[/code]. This will remove the file from the staging index but it will not modify the file in the working tree. 
(WF): Previously tracked files must be unstaged before they will be ignored by a new rule in the .gitignore file. The command to do this is [code]git rm --cached <filename>[/code]. This will remove the file from the staging index but it will not modify the file in the working tree. 
(STARTIGNORE)
(Hint):
(Subject): Ignoring Files
(Difficulty): Intermediate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 2
(Random answers): 1
(Question): The Git ‘reset’ command resets the head to a specified commit. Choose the answers that best reflect the three main reset modes and their purposes (choose 3).
(A): [code]--soft[/code] reset the HEAD and do not touch the file changes in the staging index or working tree
(B): [code]--mixed[/code] reset the HEAD and the working tree but preserve file changes in the staging index
(C): [code]--hard[/code] reset the HEAD, staging index, and working tree. File changes are discarded.
(D): [code]--mixed[/code] reset the HEAD and the staging index but preserve file changes in the working tree
(E): [code]--medium[/code] reset the HEAD and the working tree but preserve file changes in the staging index
(Correct): A,C,D
(Points): 1
(CF): The reset [code]--soft[/code] option does not affect the staging index or the working tree, the reset [code]—mixed[/code] option affects the staging index but preserves file changes in the working tree, and the reset [code]--hard[/code] option does not preserve file changes in the staging index or working tree.
(WF): The reset [code]--soft[/code] option does not affect the staging index or the working tree, the reset [code]—mixed[/code] option affects the staging index but preserves file changes in the working tree, and the reset [code]--hard[/code] option does not preserve file changes in the staging index or working tree.
(STARTIGNORE)
(Hint):
(Subject): Undoing Changes
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
