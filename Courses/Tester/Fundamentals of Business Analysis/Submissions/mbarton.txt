(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Udemy
(Course Name): Fundamentals of Business Analysis
(Course URL): https://www.udemy.com/businessanalysis/learn/v4/overview
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): A component-wise analysis of a feature to understand its difference from another feature is known as
(A): Decomposition Analysis
(B): Cost Benefit Analysis
(C): Gap Analysis
(D): Stakeholder Needs Analysis 
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA 
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category):15
(Grade style): 2
(Random answers): 0
(Question): What is a requirement?
(A): A usable representation of a need
(B): Documentation of value that the solution must provide
(C): A document (or set of documents) that specifies the solution in detail
(D): All of the above 
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 2
(Random answers): 0
(Question): True or False: Business Analysts are only needed for projects that use a waterfall model lifecycle.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 2
(Random answers): 1
(Question): Which of the below are attributes of good requirements?
(A): Clear
(B): Complete
(C): Applicable
(D): Prioritized
(E): Implementable
(F): Testable
(G): Approved 
(Correct): A,B,C,D,E,F
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Requirements that describe how a system will behave are
(A): Functional Requirements
(B): User Requirements
(C): Business Requirements
(D): Non-Functional Requirements
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which of these is not a valid reason for modeling processes?
(A): To create a new process
(B): To improve an existing process
(C): To re-engineer an existing process
(D): To reduce amount of documentation for processes
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): True or False: Swimlane Flowcharts are helpful when the project being analyzed is very process-heavy.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): During which phase of business analysis is most of the analysis and modeling typically done?
(A): Forming Phase
(B): Transforming Phase
(C): Finalizing Phase
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When would a Stakeholder Needs Analysis be performed?
(A): When stakeholders have conflicting needs
(B): When stakeholders are not located in the same office
(C): When stakeholders agree on the requirements
(D): When there are no stakeholders
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): A Decomposition Analysis can be performed on which of the following
(A): Processes
(B): Tasks
(C): Documents
(D): Users
(E): All of the above
(Correct): A,B,C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): BA
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)