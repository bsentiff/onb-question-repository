(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Joseph Duda
(Course Site): Wikipedia
(Course Name): Software testing
(Course URL): https://en.wikipedia.org/wiki/Software_testing
(Discipline): Professional
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): Finding and fixing a defect early in the product cycle is more expensive than testing a fully functional application just prior to its release.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Test early and test often. The earlier a defect is discovered, the less resources it will require to fix.
(WF): Test early and test often. The earlier a defect is discovered, the less resources it will require to fix.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which of the following refers testing the functionality of specific sections of code?
(A): Unit testing.
(B): Integration testing.
(C): Component-interface testing.
(D): System testing.
(E): Operational acceptance testing.
(Correct): A
(Points): 1
(CF): Unit testing refers to tests that verify the functionality of a specific section of code, usually at the function level.
(WF): Unit testing refers to tests that verify the functionality of a specific section of code, usually at the function level.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): You use your pet monkey to test your automated banana dispenser by randomly banging on the keyboard and seeing what happens. Your monkey could be considered a...
(A): White-box tester.
(B): Grey-box tester.
(C): Black-box tester.
(D): Test Analyst.
(E): Developer.
(Correct): C
(Points): 1
(CF): Black-box testers have no knowledge of the internal workings of a product. They generally test through the user interface.
(WF): Black-box testers have no knowledge of the internal workings of a product. They generally test through the user interface.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): Static testing refers to testing while actually executing the code.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Dynamic testing happens when executing code. Static testing is more of a review or inspection of the code.
(WF): Dynamic testing happens when executing code. Static testing is more of a review or inspection of the code.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Microsoft is about to release a new version of Windows. You are unsure if your current software product will still work when users begin upgrading. What kind of testing should you do?
(A): Regression testing.
(B): Beta testing.
(C): Performance testing.
(D): Compatibility testing.
(E): Security testing.
(Correct): D
(Points): 1
(CF): A common cause of software failure is a lack of its compatibility with other application software, operating systems, or target environments that differ greatly from the original.
(WF): A common cause of software failure is a lack of its compatibility with other application software, operating systems, or target environments that differ greatly from the original.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): You've just introduced a new feature that uses some state-of-the-art graphics technology but now some testers are reporting a sluggishness in their computers. Which type of testing would likely be most applicable to this new feature?
(A): Regression testing.
(B): Beta testing.
(C): Performance testing.
(D): Compatibility testing.
(E): Security testing.
(Correct): C
(Points): 1
(CF): Performance testing is generally executed to determine how a system or sub-system performs in terms of responsiveness and stability under a particular workload.
(WF): Performance testing is generally executed to determine how a system or sub-system performs in terms of responsiveness and stability under a particular workload.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): In an Agile project, testing is typically done when?
(A): Only after the product is completely developed.
(B): At the end of each sprint.
(C): During each sprint, side-by-side with developers.
(D): The tests are all planned and written before each sprint begins.
(E): There is no need for testing in an Agile project.
(Correct): C
(Points): 1
(CF): Testers and developers should both be part of the team and work very closely with each other. Testing should be ongoing throughout each sprint.
(WF): Testers and developers should both be part of the team and work very closely with each other. Testing should be ongoing throughout each sprint.
(STARTIGNORE)
(Hint):
(Subject): Software Testing Roles
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): A test suite is a collection of test cases.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The most common term for a collection of test cases is a test suite. The test suite often also contains more detailed instructions or goals for each collection of test cases.
(WF): The most common term for a collection of test cases is a test suite. The test suite often also contains more detailed instructions or goals for each collection of test cases.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): Load testing tries to overload the software, perhaps with a database that is too large or asking too many users to sign on at once.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Load testing is primarily concerned with testing that the system can continue to operate under a specific load.
(WF): Load testing is primarily concerned with testing that the system can continue to operate under a specific load.
(STARTIGNORE)
(Hint):
(Subject): Software Testing
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)