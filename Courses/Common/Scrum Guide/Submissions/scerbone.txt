(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): scrumguides.org
(Course Name): The Scrum Guide
(Course URL): http://www.scrumguides.org/docs/scrumguide/v1/scrum-guide-us.pdf#zoom=100
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum is a process for building products.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Scrum is not a process for building products.
(WF): Scrum is not a process for building products.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Empiricism asserts knowledge is from
(A): books.
(B): wikipedia.
(C): experience.
(D): teachers.
(E): history.
(Correct): C
(Points): 1
(CF): Empiricism asserts knowledge is from experience.
(WF): Empiricism asserts knowledge is from experience.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): intermediate
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum encourages infrequent but effective inspections.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Scrum encourages frequent inspections, without getting in the way of work.
(WF): Scrum encourages frequent inspections, without getting in the way of work.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What are attributes of a scrum team?
(A): Self organizing
(B): Cross functional
(C): Optimized
(D): Flexible
(E): Productive
(Correct): A, B, C, D, E
(Points): 1
(CF): These are all attributes of a scrum team.
(WF): These are all attributes of a scrum team.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): intermediate
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Why does scrum utilize incremental product development?
(A): To maximize feedback.
(B): To ensure a useful product is always available.
(C): To keep the product as small as possible.
(D): To avoid extra expenses.
(E): To keep track of employee work progress.
(Correct): A, B
(Points): 1
(CF): Scrum utilizes incremental product development to maximize feedback and to ensure a useful product is always available.
(WF): Scrum utilizes incremental product development to maximize feedback and to ensure a useful product is always available.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): intermediate
(Applicability): general
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who is responsible for organizing scrum meetings and leading the organization in its adoption of scrum.
(A): The development team.
(B): The scrum master.
(C): The product owner.
(D): The shareholders.
(E): The scrum team.
(Correct): B
(Points): 1
(CF): The scrum master is responsible for organizing scrum meetings and leading the organization in its adoption of scrum.
(WF): The scrum master is responsible for organizing scrum meetings and leading the organization in its adoption of scrum.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): beginner
(Applicability): general
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is allowed to change during the course of a sprint?
(A): Quality goals
(B): Scope
(C): Product backlog
(D): Product goals
(E): Functionality
(Correct): B, C, E
(Points): 1
(CF): The scope of the project, product backlog (changed by the product owner), and the functionality of the product can change during a sprint.
(WF): The scope of the project, product backlog (changed by the product owner), and the functionality of the product can change during a sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Advanced
(Applicability): general
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the maximum amount of time the daily scrum meeting can take?
(A): 5
(B): 10
(C): 15
(D): 20
(E): 25
(Correct): C
(Points): 1
(CF): The daily scrum meeting is time boxed at 15 minutes.
(WF): The daily scrum meeting is time boxed at 15 minutes.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): In the sprint review the development team must demonstrate the work they finished on the product.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): In the sprint review the development team must demonstrate the work they finished on the product to the product owner and scrum master.
(WF): In the sprint review the development team must demonstrate the work they finished on the product to the product owner and scrum master.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): intermediate
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The product backlog is an ordered list of everything that must be implemented in the first sprint.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The product backlog is an ordered list of everything that might be needed in the product.
(WF): The product backlog is an ordered list of everything that might be needed in the product.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): beginner
(Applicability): general
(ENDIGNORE)