(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Apurwa Dandekar
(Course Site): http://www.scrumguides.org/
(Course Name): The scrum Guide
(Course URL):http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-US.pdf#zoom=100
(Discipline):Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the main purpose of scrum?
(A): Building products
(B): Developing and sustaining complex products
(C): To design a product
(D): To test and rebuild the product 
(Correct): B
(Points): 1
(CF): Scrum is a framework for developing and sustaining complex products
(WF): Scrum is a framework for developing and sustaining complex products
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 2
(Random answers): 1
(Question): Why is the iterative approach employed in Scrum?
(A): To optimize predictability
(B): For maintenance
(C): To test and rebuild the product
(D): To control the risk involved in the project
(E): To create a document of tasks performed
(Correct): A, C
(Points): 2
(CF): Scrum employs an iterative, incremental approach to optimize predictability and control risk
(WF): Scrum employs an iterative, incremental approach to optimize predictability and control risk
(STARTIGNORE)
(Hint):
(Subject): Scrum Theory
(Difficulty): Beginner
(Applicability): Scrum guide
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): Which of the following ensures artifacts and progress toward a Sprint Goal to detect undesirable variances?
(A): Adaptation 
(B): Transparency
(C): Inspection
(D): Testing
(Correct): C
(Points): 1
(CF): Scrum users must frequently inspect Scrum artifacts and progress toward a Sprint Goal to detect undesirable variances.
(WF): Scrum users must frequently inspect Scrum artifacts and progress toward a Sprint Goal to detect undesirable variances.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 2
(Random answers): 1
(Question): What does scrum Team consist of?
(A): Scrum Master
(B): Client
(C): Consultant
(D): Development Team
(E): Product owner
(Correct): A, D, E
(Points): 3
(CF): The Scrum Team consists of a Product Owner, the Development Team, and a Scrum Master. 
(WF): The Scrum Team consists of a Product Owner, the Development Team, and a Scrum Master.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Moderate
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): What is a project backlog?
(A): Consists of every requirement for a system, project, or product.
(B): Consists of a detailed activities performed in the project
(C): Consists of task and its estimates
(D): Consists of product report and issue log
(Correct): A
(Points): 1
(CF): In Scrum, the product backlog is the single most important artifact. The product backlog is, in essence, an incredibly detailed analysis document, which outlines every requirement for a system, project, or product.
(WF): In Scrum, the product backlog is the single most important artifact. The product backlog is, in essence, an incredibly detailed analysis document, which outlines every requirement for a system, project, or product.
(STARTIGNORE)
(Hint):
(Subject): The scrum Team
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): What is a desired team size for the scrum development team?
(A): Greater than 10 and Less than 50
(B): Less than 100
(C): Greater than 3 and less than 9
(D): Not more than 20
(Correct): C
(Points): 1
(CF): Development Team members decrease interaction and results in smaller productivity gains. Having more than nine members requires too much coordination.
(WF): Development Team members decrease interaction and results in smaller productivity gains. Having more than nine members requires too much coordination.
(STARTIGNORE)
(Hint):
(Subject):Scrum team
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): Who has the authority to cancel the Sprint?
(A): Project Manager 
(B): Product Owner
(C): Scrum Master
(D): Client 
(Correct): B
(Points): 1
(CF): A Sprint can be cancelled before the Sprint time-box is over. Only the Product Owner has the authority to cancel the Sprint
(WF): A Sprint can be cancelled before the Sprint time-box is over. Only the Product Owner has the authority to cancel the Sprint
(STARTIGNORE)
(Hint):
(Subject): Scrum event
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): What is a sprint Backlog?
(A): The log of project task and activities performed
(B): The responsibility of each development team
(C): Product backlog items selected for this Sprint plus the plan for delivering them
(D): The error log during the development process
(Correct): C
(Points): 1
(CF):  The Product Backlog items selected for this Sprint plus the plan for delivering them is called the Sprint Backlog.
(WF):  The Product Backlog items selected for this Sprint plus the plan for delivering them is called the Sprint Backlog.
(STARTIGNORE)
(Hint):
(Subject):Sprint
(Difficulty): Moderate
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category):5
(Grade style): 0
(Random answers): 1
(Question): Multiple scrum teams can not work together 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Multiple Scrum Teams often work together on the same product. One Product Backlog is used to describe the upcoming work on the product.
(WF): Multiple Scrum Teams often work together on the same product. One Product Backlog is used to describe the upcoming work on the product.
(STARTIGNORE)
(Hint):
(Subject):Scrum backlog
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category):5
(Grade style): 0
(Random answers): 1
(Question): Which process gives opportunity for the Scrum Team to inspect itself and create a plan?
for improvements?
(A): Sprint Retrospective
(B): Sprint Inspection
(C): Sprint Adaptation
(D): Creating a Scrum Backlog
(E): Testing
(Correct): A
(Points): 1
(CF): The Sprint Retrospective is an opportunity for the Scrum Team to inspect itself and create a plan for improvements to be enacted during the next Sprint.
(WF): The Sprint Retrospective is an opportunity for the Scrum Team to inspect itself and create a plan for improvements to be enacted during the next Sprint.
(STARTIGNORE)
(Hint):
(Subject):Sprint in scrum
(Difficulty): Moderate
(Applicability): Course
(ENDIGNORE)