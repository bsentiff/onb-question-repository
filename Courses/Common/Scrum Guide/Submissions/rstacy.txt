(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category): 0

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses


For more help on adding questions, go to http://www.classmarker.com/a/help/#tests and look under the 'Import questions' section

(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Scrum.org
(Course Name): Scrum Guide
(Course URL):https://www.scrum.org/Portals/0/Documents/Scrum%20Guides/2013/Scrum-Guide.pdf
(Discipline): Professional 
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum...
(A): is lightweight
(B): only applies to coding software
(C): is simple to apply
(D): only helps developers
(Correct): A
(Points): 1
(CF): 
(WF): Scrum does not necessarily apply only to coding and developers.  Its application can be difficult to master.



(Type): multiplechoice
(Category): 5
(Grade style): 2
(Random answers): 1
(Question): Select the three pillars of empirical process control. 
(A): Inspection
(B): Transparency
(C): Adaptation
(D): Implementation
(Correct): A, B, C
(Points): 1
(CF): 
(WF): Implementation is not one of the pillars


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is an aspect of the Transparency pillar?
(A): Complaints about management are made public
(B): Everyone shares a common definition of "done"
(C): All code is accessible by anyone in the company
(D): All projects are open source
(Correct): B
(Points): 1
(CF): Correct.  Without a concrete definition of "done", different issues will be in different states but have the same listing.
(WF): Transparency transcends just coding.


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How can Scrum ensure frequent feedback from the product?
(A): Iterative and Incremental releases
(B): Releasing the full product in Beta
(C): Releasing the full product in Alpha
(D): Making sure code is deployed as soon as it is checked in
(Correct): A
(Points): 1
(CF): Correct. Rapid and frequent releases of "done" stories is the most effective way.
(WF): To get frequent feedback, there must be frequent deploys without damaging the product.



(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who manages the product backlog?
(A): The Product Owner
(B): The Scrum Master
(C): The Software Team Lead
(D): Answer not listed
(Correct): D
(Points): 1
(CF): Correct. There are many people who can manage the backlog. The Product Owner may do it, or the development team can assist. 
(WF): Managing the backlog is not limited to one person.  The Product Owner is responsible for it, but can delegate work.


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Suppose you are a developer, and a Manager of another team informs you that there a bug in your backlog that needs to be fixed immediately due to a road block.  What is a proper course of action?
(A): Address the Product Owner
(B): Fix the bug immediately
(C): Edit the backlog and promote the bug to a higher priority
(D): Wait until the next backlog grooming to address the problem
(Correct): A
(Points): 1
(CF): Correct. No one is allowed to tell the Development Team to work from a different set of requirements, and the Development Team isn’t allowed to act on what anyone else says.
(WF): While the development team should not act on what an external manager says, the Product Owner needs to be informed and their decision respected.


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): True or False: A development team must have a focused skill set.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): A development team needs all of the skills required to create an increment.


(Type): multiplechoice
(Category): 5
(Grade style): 2
(Random answers): 1
(Question): Select some of the issues with having a small (< 3) development team.
(A): There may be skill limitations
(B): Smaller productivity gains
(C): Decrease in interaction
(D): Less motivation to work
(Correct): A, B, C
(Points): 1
(CF): 
(WF): 


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which event lies outside of a sprint?
(A): Daily Scrum
(B): Sprint Review
(C): Sprint Retrospective
(D): None
(Correct): D
(Points): 1
(CF): Correct. All events are contained in the sprint.
(WF): A new Sprint begins right after one ends.  Nothing is outside of it.


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): When is a Project backlog considered "complete"?
(A): When the final items are listed as "done"
(B): When the final items are deployed into the product
(C): Never, as long as the product exists
(D): At the end of each sprint
(Correct): C
(Points): 1
(CF): 
(WF): The product backlog is constantly evolving and changing, even after deployments.