(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): SDLC
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following are stages used in the Agile methodology?
(A): Explore
(B): Adapt
(C): Learn
(D): Close
(E): Revisit
(Correct): A, B, D
(Points): 1
(CF): The Agile methodology stages are Envision, Speculate, Explore, Adapt, and Close.
(WF): The Agile methodology stages are Envision, Speculate, Explore, Adapt, and Close.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5 
(Grade style): 0
(Random answers): 1
(Question): What is a burndown chart?
(A): A chart which illustrates the work left to be completed
(B): A chart that shows the hours remaining for the project
(C): A chart that shows the funding remaining for a project
(D): A chart that shows the number of bugs that have been fixed
(E): A chart showing all of a project's use cases
(Correct): A
(Points): 1
(CF): A burndown chart illustrates work left to be completed
(WF): A burndown chart illustrates work left to be completed
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What are the phases of the PDCA process?
(A): Plan, Do, Check, Adjust
(B): Process, Do, Certify, Acquire
(C): Plot, Develop, Close, Ascertain
(D): Program, Design, Create, Aspire
(E): Plan, Develop, Confirm, Amend
(Correct): A
(Points): 1
(CF): The steps of the PDA process are Plan, Do, Check, and Adjust.
(WF): The steps of the PDA process are Plan, Do, Check, and Adjust.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following should be included in the Iteration, Milestone and Release Plan?
(A): A list of which developers will complete which features
(B): A list of all features to be completed
(C): Dates for when each feature will be completed
(D): The cost of each feature
(E): The development teams' velocity
(Correct): B, C
(Points): 1
(CF): The Iteration, Milestone and Release Plan should contain a list of all the features that will be completed, when they will be completed, and when they will be available to the business.
(WF): The Iteration, Milestone and Release Plan should contain a list of all the features that will be completed, when they will be completed, and when they will be available to the business.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): In a six-week sprint that is not the initial sprint of a project, what would you expect the rough breakdown of each phase to be?
(A): 1 week for Speculate, 5 weeks for Explore, 1 week for Adapt
(B): 2 weeks for Speculate, 2 weeks for Explore, 2 weeks for Adapt
(C): 1 week for Envision, 1 week for Explore, 5 weeks for Adapt
(D): 5 weeks for Speculate, 1 week for Explore, 1 week for Adapt
(E): 1 week for Envision, 5 weeks for Explore, 1 week for Adapt
(Correct): A
(Points): 1
(CF): The phases that occur during a sprint are Speculate, Explore, and Adapt. The majority of time will generally be allotted to Explore.
(WF): The phases that occur during a sprint are Speculate, Explore, and Adapt. The majority of time will generally be allotted to Explore.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following describe good practices for stand-up meetings?
(A): Invite other stakeholders on a weekly basis
(B): Assign a timekeeper
(C): The project manager should lead the meeting
(D): End on a positive note
(E): Each team member should be able to share necessary information in about a minute or less
(Correct): A, B, D, E
(Points): 1
(CF): All of these are true, except "project manager should lead the meeting," which does not describe the ideal project manager role during stand-ups.
(WF): All of these are true, except "project manager should lead the meeting," which does not describe the ideal project manager role during stand-ups.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): If a team is very well-versed in the Agile methodology, what is a good way to mitigate risk?
(A): Complete complex or difficult features in early sprints
(B): Move complex or difficult features to later sprints
(C): Make your sprints shorter as the project moves forward
(D): Lengthen the Speculate phase of each sprint
(E): Distribute complex or difficult features throughout all sprints
(Correct): A
(Points): 1
(CF): A team that is familiar with Agile can lower project risk by tackling difficult features first.
(WF): A team that is familiar with Agile can lower project risk by tackling difficult features first.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the generally accepted upper bound on the number of members in an Agile team?
(A): 5
(B): 12
(C): 15
(D): 20
(E): 2
(Correct): C
(Points): 1
(CF): An Agile team should generally not be larger than 15 people.
(WF): An Agile team should generally not be larger than 15 people.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): During which phase should team norms be established?
(A): Organize
(B): Plan
(C): Outline
(D): Speculate
(E): Envision
(Correct): E
(Points): 1
(CF): Team norms should be established in the first phase of a project, Envision.
(WF): Team norms should be established in the first phase of a project, Envision.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the role of the Project Manager in the Agile methodology?
(A): Make all key decisions
(B): Guide and clear roadblocks for the team
(C): Develop effective unit tests
(D): Estimate tasks
(E): Maintain rigid control over the team
(Correct): B
(Points): 1
(CF): Project managers should observe, guide, and clear roadblocks for the development team, which requires giving up some level of control as compared to other development methods.
(WF): Project managers should observe, guide, and clear roadblocks for the development team, which requires giving up some level of control as compared to other development methods.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)