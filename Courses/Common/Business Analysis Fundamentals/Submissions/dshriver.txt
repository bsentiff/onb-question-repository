(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Derrick Shriver
(Course Site): Lynda
(Course Name): Business Analysis Fundamentals
(Course URL): http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Business Analysis
(ENDIGNORE)

(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): When validating a requirement with 10 reviewers, it is not necessary for all to believe the requirement would bring business value.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 75% of reviewers should believe the requirement would bring business value.
(WF): 75% of reviewers should believe the requirement would bring business value.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): Acceptance critiera define a fixed finish line for all to see and strive for.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): This course focuses on fixed-scope projects.
(WF): This course focuses on fixed-scope projects.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): Lessons learned should include both things to avoid and things to repeat.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Lessons learned are to help validate successful techniques as well as make improvements.
(WF): Lessons learned are to help validate successful techniques as well as make improvements.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): When making observations during requirements gathering, a business analyst should... (select all that apply)
(A): Look for swivel technology
(B): Look for manual steps
(C): Concentrate exclusively on the solution that the stakekeholders describe
(D): Design the solution
(Correct): A,B
(Points): 1
(CF): A goal of the requirements process is to define "what" is needed to be done, not the solution or "how" it should be done.
(WF): A goal of the requirements process is to define "what" is needed to be done, not the solution or "how" it should be done.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): When validating requirements, peer review can be very productive but can be informal.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Peer review is informal; stakeholder review is formal.
(WF): Peer review is informal; stakeholder review is formal.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)