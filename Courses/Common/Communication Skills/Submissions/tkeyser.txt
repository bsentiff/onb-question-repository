(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): udemy
(Course Name): communications skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): How many meetings do people consider to be a waste of time?
(A): 3/4
(B): Half
(C): 1/4
(D): all
(Correct): B
(Points): 1
(CF): The average person considers half of their meetings to be a waste of time.
(WF): The average person considers half of their meetings to be a waste of time.
(STARTIGNORE)
(Hint):
(Subject): Communications
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What percentage of people do other work while in meetings?
(A): 73%
(B): 24%
(C): 90%
(D): 10%
(Correct): A
(Points): 1
(CF): 73% of people do other work while in meetings.
(WF): 73% of people do other work while in meetings.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): A great facilitator can make all the difference in a meeting.
(A): True	
(B): False
(Correct): True
(Points): 1
(CF): A great facilitator can make all the difference in a meeting.
(WF): A great facilitator can make all the difference in a meeting.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What four things should you do when you go to a meeting?
(A): Sit up, take notes on computer, look at the chairperson, listen
(B): Slouch, don't take notes, interrupt, turn phone on
(C): Sit furthest away from the presenter, have side conversations, daydream, type loudly on computer
(D): Sit up, seat up, square up, hands up
(Correct): D
(Points): 1
(CF): You should always do these four things in meetings: Sit up (straight back, head up, no slouching, seat up (don't demote yourself physically), square up (turn towards people when addressing them), hands up (rest your hands and forearms on the table)
(WF): You should always do these four things in meetings: Sit up (straight back, head up, no slouching, seat up (don't demote yourself physically), square up (turn towards people when addressing them), hands up (rest your hands and forearms on the table)
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What person should you not be when sitting in a meeting?
(A): Sales guy
(B): A person taking notes
(C): The guy paying attention
(D): A person giving feedback
(Correct): A
(Points): 1
(CF): You should not be the sales guy in a meeting trying to sell more stuff.
(WF): You should not be the sales guy in a meeting trying to sell more stuff.
(STARTIGNORE)
(Hint):
(Subject): Communications Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): A cell phone should be used when conducting a conference call.
(A): True
(B): False
(Correct): False
(Points): 1
(CF): A cell phone should not be used when conducting a conference call, mainly because they are unreliable.
(WF): A cell phone should not be used when conducting a conference call, mainly because they are unreliable.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Don't read materials that are given out during the call.
(A): True
(B): False
(Correct): True
(Points): 1
(CF): People can read, don't read the materials on the call. This includes handouts and information on slides.
(WF): People can read, don't read the materials on the call. This includes handouts and information on slides.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Slides are good to use and shouldn't be avoided during a conference call.
(A): True
(B): False
(Correct): False 
(Points): 1
(CF): Slides should be avoided during calls.
(WF): Slides should be avoided during calls.
(STARTIGNORE)
(Hint):
(Subject): Communications Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): How many emails does the average person recieve in a week?
(A): 1,000
(B): 200
(C): 304
(D): 100
(E): 720
(Correct): C
(Points): 1
(CF): The average person recieves 304 emails a week.
(WF): The average person recieves 304 emails a week.
(STARTIGNORE)
(Hint):
(Subject): Communications Skills eMail
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): True or False
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): A consultant might see you more with the human brain than an executive. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A consultant might see you more with a lizard (fight or flight) brain than an executive with the human side who hopes you understand their strategy.
(WF): A consultant might see you more with a lizard (fight or flight) brain than an executive with the human side who hopes you understand their strategy.
(STARTIGNORE)
(Hint):
(Subject): Communications Skills People
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)