(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Udemy
(Course Name): Communication Skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/learn/v4/overview
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 2
(Random answers): 0
(Question): When interfacing with other roles as a consultant which roles will you need to convince of your technical skills?
(A): Project Managers
(B): Other Consultants
(C): Managers
(D): Executives
(Correct): A,B
(Points): 1
(CF): Project Managers and any other consultants you might work with need to know you are technically capable and/or superior. Managers and Executives are more concerned with the bigger picture.
(WF): Project Managers and any other consultants you might work with need to know you are technically capable and/or superior. Managers and Executives are more concerned with the bigger picture.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 2
(Random answers): 1
(Question): Which item(s) are typically included in an elevator pitch?
(A): A detailed list of problems you are currently facing on the project.
(B): Your role on the project.
(C): The company you work for.
(D): Gossip about project participants.
(E): A succinct summary of the project.
(Correct): B,C,E
(Points): 1
(CF): Good job!
(WF): Elevator pitches need to be brief (think 20,000 ft overview) and should never include gossip or details that an executive would not be concerned with.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 2
(Random answers): 1
(Question): Which option is the best approach to dealing with adversarial co-workers?
(A): Punish them
(B): Tattle on them to a Manager and let them deal with it
(C): Let them ruin the project so they can be blamed
(D): Lash out at them in anger
(E): Convert them to an ally by improving your relationship
(Correct): E
(Points): 1
(CF): Good job!
(WF): Though your instinct may be to fight fire with fire, you should always act professionally and maintain your cool.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

