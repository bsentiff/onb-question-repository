(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): udemy
(Course Name): Communication Skills
(Course URL): https://www.udemy.com/consulting-skills-series-communication/
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Which of the following are domains of communication?
(A): Emails
(B): Computers
(C): Pencil and Paper
(D): Presentations
(E): People
(Correct): A,D,E
(Points): 1
(CF): The five domains of communication are: Meetings, conference calls, presentations, emails, and people.
(WF): The five domains of communication are: Meetings, conference calls, presentations, emails, and people.
(STARTIGNORE)
(Hint): What are the five main topic covered in the course?
(Subject): Communication Skills
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): You should address the "chair" when possible in their meeting.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You should always try to address the "chair" when it is their meeting.
(WF): You should always try to address the "chair" when it is their meeting.
(STARTIGNORE)
(Hint): 
(Subject): Communication Skills - Meetings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): You should not resend the agenda and materials the day of a conference call.
(A): True
(B): False 
(Correct): B
(Points): 1
(CF): You should always resend the agenda and materials the day of a conference call.
(WF): You should always resend the agenda and materials the day of a conference call.
(STARTIGNORE)
(Hint): 
(Subject): Communication Skills - Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): You should start your conference call by first
(A): setting ground rules.
(B): reviewing the agenda.
(C): introducing people.
(D): calling the meeting to order.
(E): ensuring everyone has materials.
(Correct): D
(Points): 1
(CF): You should start every conference call by calling the meeting to order.
(WF): You should start every conference call by calling the meeting to order.
(STARTIGNORE)
(Hint): 
(Subject): Communication Skills - Conference Calls
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): Asking for frequent feedback is a bad idea during conference calls.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You need to ask for frequent feedback during conference calls.
(WF): You need to ask for frequent feedback during conference calls.
(STARTIGNORE)
(Hint): 
(Subject): Communication Skills - Conference Calls
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What are things you must include in professional emails?
(A): Humor
(B): CC and BCC
(C): An effective subject line
(D): Deadlines
(E): A Signature
(Correct): C,D,E
(Points): 1
(CF): An effective subject line, including any deadlines, and a signature are are essential parts of a professional email.
(WF): An effective subject line, including any deadlines, and a signature are are essential parts of a professional email.
(STARTIGNORE)
(Hint):
(Subject): Communication Skills - Emails
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): There is no need to include "please" and "thank you" in a professional email.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You should always include "please" and "thank you" (when applicable) in professional emails.
(WF): You should always include "please" and "thank you" (when applicable) in professional emails.
(STARTIGNORE)
(Hint):
(Subject): Communications Skills - Emails
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When presenting, your goal is to
(A): entertain with little text.
(B): entertain with pictures.
(C): inform with little text.
(D): inform with pictures.
(E): entertain and Inform with pictures.
(Correct): C
(Points): 1
(CF): Your goal should be to inform with little text when presenting.
(WF): Your goal should be to inform with little text when presenting.
(STARTIGNORE)
(Hint):
(Subject): Communication Skills - Presentations
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): When presenting you should use clip-art instead of pictures.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You should use pictures instead of clip-art when presenting.
(WF): You should use pictures instead of clip-art when presenting.
(STARTIGNORE)
(Hint):
(Subject): Communications Skills - Presentations
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 1
(Question): What should you do when confronted with adversity?
(A): Listen carefully
(B): Repeat yourself
(C): Avoid direct questions
(D): Repeat it back
(E): Answer directly
(Correct): A,D,E
(Points): 1
(CF): When confronted you should listen carefully, repeat it back, and answer directly.
(WF): When confronted you should listen carefully, repeat it back, and answer directly.
(STARTIGNORE)
(Hint):
(Subject): Communications Skills - People
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
