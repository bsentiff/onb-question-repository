(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): discover-devtools.codeschool.com
(Course Name): Explore and Master Chrome DevTools
(Course URL): http://discover-devtools.codeschool.com/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When is it a good idea to load JavaScript files before rendering the page?
(A): When the JavaScript files affect the behavior of the page.
(B): When the JavaScript files affect the layout of the page.
(C): When the JavaScript files are small.
(D): When the JavaScript files are large.
(E): When the HTML is large. 
(Correct): B
(Points): 1
(CF): It is a good idea to load the JavaScript files before rendering the page when they affect the layout of the page.
(WF): It is a good idea to load the JavaScript files before rendering the page when they affect the layout of the page.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What would the following code do when entered into the Chrome DevTools console? $0
(A): It will print out any element with an id of '0'.
(B): It will print out any element with a class of '0'.
(C): It will print out the last selected element.
(D): It will print out the first selected element.
(E): It will print out all elements with a name of '0'.
(Correct): C
(Points): 1
(CF): The code will print out the last selected element.
(WF): The code will print out the last selected element.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): 
(Applicability): 
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The following JavaScript code (or console code) will display an error when called: console.warn("There is an error!");
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The code will display a warning when called, not an error.
(WF): The code will display a warning when called, not an error.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): A script with the 'async' attribute will be run as soon as it is available.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A script with the 'async' attribute will be run as soon as it is available.
(WF): A script with the 'async' attribute will be run as soon as it is available.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When a change is made to an element in Chrome DevTools the changes are made in the source files as well.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Changes made to elements in Chrome DevTools do not change the source files until the changes are exported and replace the original source files.
(WF): Changes made to elements in Chrome DevTools do not change the source files until the changes are exported and replace the original source files.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which tab in Chrome DevTools can help you find memory leaks?
(A): Elements
(B): Network
(C): Timeline
(D): Profiles
(E): Resources
(Correct): D
(Points): 1
(CF): The profiles tab can help you find memory leaks.
(WF): The profiles tab can help you find memory leaks.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When an error is raised Chrome DevTools by default breaks until the error is dealt with or stepped over.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Chrome DevTools will not break by default when an error is encountered however there is an option to do just that.
(WF): Chrome DevTools will not break by default when an error is encountered however there is an option to do just that.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): If requested resources are labelled as cached what is true about those resources?
(A): They were downloaded from a CDN.
(B): They were downloaded from the server.
(C): The resources contain cache information.
(D): The local resources were checked against the server's resource list, they were identical, and the cached version was used.
(Correct): D
(Points): 1
(CF): Before a resource is downloaded the computer checks to see if it already has the file, if it does the cached version is used.
(WF): Before a resource is downloaded the computer checks to see if it already has the file, if it does the cached version is used.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When a resource has a '404' error, what does this mean?
(A): The resource could not be found at the location specified.
(B): The resource contents did not match what the server expected.
(C): The resource took to long to load and the connection timed out.
(D): The resource failed to be requested properly.
(E): The resource was no longer needed by the server.
(Correct): A
(Points): 1
(CF): A resource with a '404' error indicates that the resource could not be found at the locations specified.
(WF): A resource with a '404' error indicates that the resource could not be found at the locations specified.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When analyzing page performance what affect does low 'FPS' have.
(A): It causes the page to seem unresponsive.
(B): It causes animations and transitions to become 'jumpy' or no longer smooth.
(C): It prevents the page from loading.
(D): It breaks all button and form functionality.
(Correct): A, B
(Points): 1
(CF): When a page has low 'FPS' it will seem unresponsive and animations/transitions will become 'jumpy' or no longer smooth.
(WF): When a page has low 'FPS' it will seem unresponsive and animations/transitions will become 'jumpy' or no longer smooth.
(STARTIGNORE)
(Hint):
(Subject): Explore and Master Chrome DevTools
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)