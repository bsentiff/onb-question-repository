==========
Objective: Apply the box model to generate layout effects (margins, padding and borders).
==========

[[
You are building a table-less form that places field labels aligned with the
input fields.  Here is a sample of the HTML for this form:

<pre>&LT;form&GT;
&LT;label&GT;Username&LT;/label&GT; &LT;input type="text" name="name"&GT; &LT;br/&GT;
&LT;label&GT;E-Mail&LT;/label&GT; &LT;input type="text" name="email"&GT; &LT;br/&GT;
&LT;label&GT;&nbsp;&LT;/label&GT; &LT;button type="submit"&GT;Go&LT;/button&GT;
&LT;/form&GT;</pre>

This is the wireframe for this form created by the UI designer:

<img src='CSS-Q2-scenario.png' title='Form wireframe from UI designer' />

What is the CSS code that you would use to style this HTML to look like the UI design?
]]
1: <pre>
label {
	align: right;
	margin: 5px;
	width: 150px;
}
input, button {
	align: left;
	margin: 5px;
}
</pre>
*2: <pre>
label {
	float: left;
	margin: 5px;
	width: 150px;
	text-align: right;
}
input, button {
	float: left;
	margin: 5px;
}
form br {
	clear: left;
}
</pre>
3: <pre>
label {
	align: left;
	margin: 5px;
	width: 150px;
	text-align: right;
}
input, button {
	align: left;
	margin: 5px;
}
form br {
	clear: alignment;
}
</pre>
4: <pre>
label {
	float: left;
	margin: 5px;
	width: 150px;
	text-align: right;
}
input, button {
	float: left;
	margin: 5px;
}
form br {
	clear: float;
}
</pre>


==========
Objective: Given a set of rules and an element scenario,
 identify which rule takes precedence (ie, evaluate the specificity of a rule).
==========

[[
Given the following CSS rules:
<pre>
body { color: black }
nav { color: light-blue }
a.submenu { color: red }
nav li a { color: yellow }
</pre>

And given the following HTML element structure:
<pre>
&LT;body&GT;
  &LT;nav&GT;
    &LT;ul&GT;
      &LT;li&GT;&LT;a id='homeBtn'&GT;Home&LT;/a&GT;&LT;/li&GT;
      &LT;li&GT;&LT;a class='submenu'&GT;Reports&LT;/a&GT;&LT;/li&GT;
    &LT;/ul&GT;
  &LT;/nav&GT;
&LT;body&GT;
</pre>

What is the color of the Reports navigation button?
]]
*1: red
2: black
3: yellow
4: light-blue
5: defaulted to browser setting

[[NOTE: John G would like to see a question on "precedence across external, internal and inline rule"
]]
